import React, { Component } from 'react';
import './menu.css';
import img from './image.png';
import {NavLink} from 'react-router-dom';
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
//import {Button, SideNav ,SideNavItem,Icon,Collapsible,CollapsibleItem} from 'react-materialize'
import { Menu, Icon, Button } from 'antd';
import "antd/dist/antd.css";
import {SelectMenuItem } from '../../actions/MenuAction'

const { SubMenu } = Menu;

class AppMenu extends Component {
  constructor(props){
    super(props)
    if(this.props.isRTL){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }
  }

  state = {
    collapsed: false,
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
     const {goTo,height} = this.props;
     console.log("GO TO   ",goTo)
    const x  = !this.props.select;
    return (
      <div style={{direction:"rtl"}} >
    
         <Menu  
         style={{position:'fixed'}}       
          //defaultSelectedKeys={[this.props.key]}
          //defaultOpenKeys={['sub1']}
          mode="inline"
          theme="dark"
          inlineCollapsed={this.props.select}
        >
          <Menu.Item  style={{marginTop: "16px"}}
              onClick={()=>{
               
              this.props.SelectMenuItem(0,x)
              }} >
              <Icon type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'} />
              
          </Menu.Item>

          <Menu.Item
            style={{}}
            onClick={()=>{
              goTo.push('/Dashboard')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem(1,true)
            }}
            key="1" style={{marginBottom:6}}>
            <Icon type="home"   style={{fontSize:20}} />
              <span  >{allStrings.home}</span>
          </Menu.Item>       

          <SubMenu
            key="sub1"
            title={
              <span>
                <Icon  type="usergroup-add" />
                <span>{allStrings.users}</span>
              </span>
            }
          >
            <Menu.Item
           onClick={()=>{
            //alert("")
            goTo.push('/users')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(2,true)
          }}
           key="2" style={{marginBottom:6}}>
          <Icon type="usergroup-add" style={{fontSize:20}} />
            <span  >{allStrings.users}</span>
          </Menu.Item>
          <Menu.Item
           onClick={()=>{
            //alert("")
            goTo.push('/sales-man')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(3,true)
          }}
           key="3" style={{marginBottom:6}} >
            <Icon type="user-delete" style={{fontSize:20}} />
            <span>{allStrings.salesmen}</span>
          </Menu.Item>

          </SubMenu>
         
          <SubMenu
            key="sub2"
            title={
              <span>
                <Icon type="appstore" />
                <span>{allStrings.categories}</span>
              </span>
            }
          >
             <Menu.Item
              onClick={()=>{
                goTo.push('/category')
                //alert("")
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem('7',true)
              }}
              key="7" style={{marginBottom:6}}>        
              <Icon type="appstore"   style={{fontSize:20}} />
                <span  >{allStrings.categories}</span>
              </Menu.Item>

              <Menu.Item
              onClick={()=>{
                goTo.push('/services category')
                //alert("")
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem('10',true)
              }}
              key="7" style={{marginBottom:6}}>        
              <Icon type="book"   style={{fontSize:20}} />
                <span  >{allStrings.serviedCategories}</span>
              </Menu.Item>
          </SubMenu>

          <SubMenu
            key="sub3"
            title={
              <span>
                <Icon type="hourglass" />
                <span>{allStrings.dateAndTime}</span>
              </span>
            }
          >

      <Menu.Item
        onClick={()=>{
          goTo.push('/year')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('112',true)
        }}
         key="7" style={{marginBottom:6}}>        
        <Icon type="number"   style={{fontSize:20}} />
          <span  >{allStrings.years}</span>
        </Menu.Item>


        <Menu.Item
        onClick={()=>{
          goTo.push('/time')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('17',true)
        }}
         key="7" style={{marginBottom:6}}>        
        <Icon type="hourglass"   style={{fontSize:20}} />
          <span  >{allStrings.time}</span>
        </Menu.Item>
            
          </SubMenu>

        <Menu.Item
        onClick={()=>{
          goTo.push('/bank')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('111',true)
        }}
         key="7" style={{marginBottom:6}}>        
        <Icon type="area-chart"   style={{fontSize:20}} />
          <span  >{allStrings.banks}</span>
        </Menu.Item>

        <Menu.Item
        onClick={()=>{
          goTo.push('/shops')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('6',true)
        }}
         key="6" style={{marginBottom:6}}>        
        <Icon type="bank"   style={{fontSize:20}} />
          <span  >{allStrings.shops}</span>
        </Menu.Item>

        
    
        <Menu.Item
        onClick={()=>{
          goTo.push('/terms')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('20',true)
        }}
         key="7" style={{marginBottom:6}}>        
        <Icon type="container"   style={{fontSize:20}} />
          <span  >{allStrings.terms}</span>
        </Menu.Item>

        <Menu.Item 
          onClick={()=>{
            //alert("")
            goTo.push('/Contactus')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(5,true)
          }}
          key="5"style={{marginBottom:6}}>
            <Icon type="wechat"  style={{fontSize:20}}/>
            <span>{allStrings.contactUS}</span>
          </Menu.Item>

         


        <Menu.Item
        onClick={()=>{
          goTo.push('/type')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('112',true)
        }}
         key="7" style={{marginBottom:6}}>        
        <Icon type="info"   style={{fontSize:20}} />
          <span  >{allStrings.types}</span>
        </Menu.Item>


       

        

       



       

        <Menu.Item
        onClick={()=>{
          goTo.push('/coupon')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('9',true)
        }}
         key="9" style={{marginBottom:6}}>        
        <Icon type="red-envelope"   style={{fontSize:20}} />
          <span  >{allStrings.coupons}</span>
        </Menu.Item>


          <Menu.Item
          onClick={()=>{
            //alert("")
            goTo.push('/pendingOrder')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(4,true)
          }}
           key="4" 
           style={{marginBottom:6}}>
            <Icon type="snippets" style={{fontSize:20}}/>
            <span>{allStrings.orders}</span>
          </Menu.Item>

          <Menu.Item 
           onClick={()=>{
            //alert("")
            goTo.push('/reports')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(6,true)
          }}
          key="6" style={{marginBottom:6}}>
            <Icon type="ordered-list" style={{fontSize:20}} />
            <span>{allStrings.reports}</span>
          </Menu.Item>

          
        </Menu>

      </div>

     /* <SideNav 
      trigger={<Button className='menu-btn'><Icon>menu</Icon></Button>}
      options={{ closeOnClick: true }}
      >
        <SideNavItem  userView
          user={{
            //background: 'img',
            
            image: require('../../assets/images/qstLogo2.png'),
            name: 'Qasat Store',
          }}
        />
        <br></br>
        <SideNavItem divider style={{background:"#333"}} />
        <br></br>

        <NavLink to="/Dashboard">
            <SideNavItem waves href='#' icon='dashboard'>
                {allStrings.dashboard}
            </SideNavItem>
        </NavLink>

        <Collapsible>
          <CollapsibleItem header={allStrings.user} icon='supervised_user_circle'>
          <NavLink to='/users'>
            <SideNavItem waves icon='group'>
            {allStrings.user}
            </SideNavItem>
        </NavLink>

        <NavLink to='/sales-man'>
            <SideNavItem waves icon='group'>
            {allStrings.salesman}
            </SideNavItem>
        </NavLink>

          </CollapsibleItem>
        </Collapsible>

        <NavLink to='/premiums'>
            <SideNavItem waves icon='add_to_photos'>
            {allStrings.permiums}
            </SideNavItem>
        </NavLink>

        <Collapsible>
          <CollapsibleItem header={allStrings.order} icon='add_shopping_cart'>
          
            <NavLink to='/acceptOrder'>
                <SideNavItem waves icon='local_mall'>
                    {allStrings.acceptOrders}
                </SideNavItem>
            </NavLink>
            <NavLink to='/pendingOrder'>
              <SideNavItem waves icon='local_mall'>
              {allStrings.pendingOrder}
              </SideNavItem>
            </NavLink>
            <NavLink to='/onTheWayOrder'>
              <SideNavItem waves icon='local_mall'>
              {allStrings.onTheWayORder}
              </SideNavItem>
            </NavLink>
            <NavLink to='/deliverdOrder'>
              <SideNavItem waves icon='local_mall'>
              {allStrings.deliverdOrder}
              </SideNavItem>
            </NavLink>
          </CollapsibleItem>
        </Collapsible>
        <NavLink to='/products'>
          <SideNavItem waves icon='shopping_basket'>
          {allStrings.product}
          </SideNavItem>
        </NavLink>
        <NavLink to='/category'>
            <SideNavItem waves icon='turned_in_not'>
            {allStrings.category}
            </SideNavItem>
        </NavLink>
        <NavLink to='/Ads'>
            <SideNavItem waves icon='add_to_queue'>
            {allStrings.ads}
            </SideNavItem>
        </NavLink>
        <NavLink to='/partners'>
            <SideNavItem waves icon='group'>
            {allStrings.partners}
            </SideNavItem>
        </NavLink>
        <NavLink to='/branch'>
            <SideNavItem waves icon='widgets'>
            {allStrings.branches}
            </SideNavItem>
        </NavLink>
       
        <NavLink to='/reports'>
              <SideNavItem waves icon='account_circle'>
              {allStrings.reports} 
              </SideNavItem>
        </NavLink>
<br></br>
<br></br>
        
    </SideNav>
        */
    );
  }
}
const mapToStateProps = state => ({
  isRTL: state.lang.isRTL,
  currentUser: state.auth.currentUser,
  key: state.menu.key,
  select: state.menu.select
})

const mapDispatchToProps = {
  SelectMenuItem,
}

export default connect(mapToStateProps,mapDispatchToProps) (AppMenu);
