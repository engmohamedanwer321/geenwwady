import React, { Component } from 'react';
import './table.css';

import { MDBDataTable } from 'mdbreact';


const data = {
  columns: [
    {
      label: 'Name',
      field: 'name',
    
    },
    {
      label: 'Position',
      field: 'position',
     
    },
    {
      label: 'Office',
      field: 'office',
      
    },
    {
      label: 'Age',
      field: 'age',
     
    },
    {
      label: 'Start date',
      field: 'date',
     
    },
    {
      label: 'Salary',
      field: 'salary',
     
    }
  ],
  rows: [
    {
      name: 'Jonas Alexander',
      position: 'Developer',
      office: 'San Francisco',
      age: '30',
      date: '2010/07/14',
      salary: '$86'
    },
    {
      name: 'Shad Decker',
      position: 'Regional Director',
      office: 'Edinburgh',
      age: '51',
      date: '2008/11/13',
      salary: '$183'
    },
    {
      name: 'Michael Bruce',
      position: 'Javascript Developer',
      office: 'Singapore',
      age: '29',
      date: '2011/06/27',
      salary: '$183'
    },
    {
      name: 'Donna Snider',
      position: 'Customer Support',
      office: 'New York',
      age: '27',
      date: '2011/01/25',
      salary: '$112'
    }
  ]
};

export class DatatablePage extends React.Component {

  render(){
    return (
      <MDBDataTable
        responsive
        striped
        bordered
        small
        data={data}
      />
    );
  }
}


export default DatatablePage;