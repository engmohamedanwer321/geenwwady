import React from 'react';
import Menu from '../../components/menu/menu';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './services category.css';
import { Skeleton, message,Modal, Form, Icon, Input, Button,Popconfirm,Select} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class ServicesCategory extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedCategory:null,
     categories:[],
     file:null,
     loading:true,
     tablePage:0,
     }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
  }
    //submit form
    flag = -1;
    getCategories = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}categories?services=true&page=${page}&limit=20`)
      .then(response=>{
        console.log("ALL Categories    ",response.data)
        //console.log(response.data)
        if(deleteRow){
          this.setState({tablePage:0})
         }
        this.setState({categories:deleteRow?response.data.data:[...this.state.categories,...response.data.data],loading:false})
      })
      .catch(error=>{
        //console.log("ALL Categories ERROR")
        //console.log(error.response)
        this.setState({loading:false})
      })
    }
    componentDidMount(){
      this.getCategories(1,true)
    }
    OKBUTTON = (e) => {
      this.deleteCategory()
     }

     deleteCategory = () => {
       let l = message.loading(allStrings.wait, 2.5)
       axios.delete(`${BASE_END_POINT}categories/${this.state.selectedCategory}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success(allStrings.deleteDone, 2.5))
           this.getCategories(1,true)
           this.flag = -1
       })
       .catch(error=>{
          // console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            //console.log('Received values of form: ', values);
            var form = new FormData();
            form.append('img',this.state.file);
            
            form.append('categoryname', values.categoryname);
            form.append('arabicname', values.arabiccategoryname);
            form.append('type', values.type);
            form.append('arabicType', values.arabicType);
            form.append('description', values.description);
            form.append('arabicDescription', values.arabicDescription);
            form.append('services', true)
            let l = message.loading(allStrings.wait, 2.5)
            axios.post(`${BASE_END_POINT}categories`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.addDone, 2.5));
                this.setState({ modal1Visible:false,file:null });
                this.fileInput.value = "";
                this.getCategories(1,true)
                this.flag = -1;
                this.props.form.resetFields()
            })
            .catch(error=>{
                console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })
          }
        });
        
      }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
         //upload props
         const props = {
            name: 'file',
            action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
            headers: {
              authorization: 'authorization-text',
            },
            onChange(info) {
              if (info.file.status !== 'uploading') {
                //console.log(info.file, info.fileList);
              }
              if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
              } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
              }
            },
          };
          //end upload props
          let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
        
          let list =this.state.categories.map((val,index)=>[
            val.id,val.categoryname,val.arabicname,""+val.type,""+val.arabicType,
            ""+val.description,""+val.arabicDescription,
            val.services?allStrings.yes:allStrings.no,
            controls
          ])

          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const {select} = this.props;
      return (
          <div>
              <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
              <Tables
               columns={this.state.loading?['loading...']: [allStrings.id,allStrings.EnglishCategoryName,allStrings.ArabicCategoryName,""+allStrings.EnglishCategoryType,""+allStrings.ArabicCategoryType,allStrings.englishDescribtion,allStrings.arabicDescribtion, allStrings.service, allStrings.remove]} 
              title={allStrings.categoryTable}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!=8){
                  
                  //console.log(this.state.categories[cellMeta.rowIndex])
                  this.props.history.push('/CategoryInfo',{flag:true, data:this.state.categories[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex==8){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedCategory:id})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getCategories(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addcategory}</Button>
              <Modal
                    title={allStrings.add}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                        {getFieldDecorator('categoryname', {
                            rules: [{ required: true, message: 'Please enter category name' }],
                        })(
                            <Input placeholder={allStrings.EnglishCategoryName}/>
                        )}
                        </Form.Item>

                        <Form.Item>
                        {getFieldDecorator('arabiccategoryname', {
                            rules: [{ required: true, message: 'Please enter abrabic category name' }],
                        })(
                            <Input placeholder={allStrings.ArabicCategoryName}/>
                        )}
                        </Form.Item>
                        
                        <Form.Item>
                        {getFieldDecorator('type', {
                            rules: [{ required: true, message: 'Please enter category type by english' }],
                        })(
                            <Input placeholder={allStrings.EnglishCategoryType}/>
                        )}
                        </Form.Item>

                        <Form.Item>
                        {getFieldDecorator('arabicType', {
                            rules: [{ required: true, message: 'Please enter category type by arabic' }],
                        })(
                            <Input placeholder={allStrings.ArabicCategoryType}/>
                        )}
                        </Form.Item>

                        <Form.Item>
                        {getFieldDecorator('description', {
                            rules: [{ required: true, message: 'Please enter english description' }],
                        })(
                            <Input placeholder={allStrings.englishDescribtion}/>
                        )}
                        </Form.Item>

                        <Form.Item>
                        {getFieldDecorator('arabicDescription', {
                            rules: [{ required: true, message: 'Please enter Arabic Description' }],
                        })(
                            <Input placeholder={allStrings.arabicDescribtion}/>
                        )}
                        </Form.Item>

                       {/* <Form.Item>
                          {getFieldDecorator('services', {
                            rules: [{ required: true, message: 'Please enter gender' }],
                        })(
                            <Select labelInValue  
                            placeholder={allStrings.service}
                            style={{ width: '100%'}}>
                                <Option value="true">{allStrings.yes}</Option>
                                <Option value="false">{allStrings.no}</Option>
                            </Select>
                        )}
                        </Form.Item>
                        */}
                        <Form.Item>
                        {getFieldDecorator('img', {
                            rules: [{ required: true, message: 'Please upload img' }],
                        })(
                          <input  ref={ref=> this.fileInput = ref} style={{width:200,margin:'auto',backgroundColor:'transparent',borderRadius:10}} type="file" onChange= {this.onChange} multiple></input>
                        )}
                            
                        </Form.Item>
                        
                        
                    </Form>
                </Modal>
              </div> 
            </div>
             <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(ServicesCategory = Form.create({ name: 'normal_login' })(ServicesCategory));
