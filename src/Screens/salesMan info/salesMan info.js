import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './salesMan info.css';
import {Input} from 'react-materialize';
import "antd/dist/antd.css";
 import { Modal,Form ,Select ,message,DatePicker} from 'antd';
 import axios from 'axios';
 import {BASE_END_POINT} from '../../config/URL'
import {Table} from 'react-materialize'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'



class SalesManInfo extends React.Component {
  page=1;
  type=null;

      state = {
        modal1Visible: false,
        salesMan:this.props.location.state.data,
        file:null,
        file2:null,
        file3:null,
        ordermodal:false,
        orders:[],
        loading:false,
        selectFlag:false,
        transportImages:this.props.location.state.data.transportImages.split(','),
        transportLicense:this.props.location.state.data.transportLicense.split(',')

      }

      constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }

      componentDidMount()
       {
           //this.getOrders(this.page)
       }
 
       getOrders= (page,type,reload) => {
        console.log("page  ",page)
        axios.get(`${BASE_END_POINT}orders?salesMan=${this.state.salesMan.id}&status=${type}&page=${page}&limit={20}`)
        .then(response=>{
          console.log("ALL orders")
          console.log(response.data.data)
          this.setState({selectFlag:true, orders:reload?response.data.data : [...this.state.orders,...response.data.data],loading:false})
        })
        .catch(error=>{
          console.log("ALL orders ERROR")
          console.log(error.response)
          this.setState({loading:false})
        })
      }

      onChange = (e) => {
        this.setState({file:e.target.files[0],
          //showFile:URL.createObjectURL(e.target.files[0])
        });      
      }
  
      onChange2 = (e) => {
        this.setState({file2:e.target.files,
          //showFile2:URL.createObjectURL(e.target.files[0])
        });
      }
  
      onChange3 = (e) => {
        this.setState({file3:e.target.files,
         // showFile3:URL.createObjectURL(e.target.files[0])
        });     
      }

    activeUser = (active) => {
        let uri ='';
        if(active){
         uri = `${BASE_END_POINT}${this.state.salesMan.id}/active`
        }else{
         uri = `${BASE_END_POINT}${this.state.salesMan.id}/disactive`
        }
       let l = message
        .loading(allStrings.wait, 2.5)
        axios.put(uri,{},{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
        .then(response=>{
            // console.log('done')
             if(active){
                 
                 l.then(() => message.success(allStrings.unblockDone, 2.5))
                 
             }else{
             
                l.then(() => message.success(allStrings.blockDone, 2.5))
             }
             this.props.history.goBack()
        })
        .catch(error=>{
        // console.log('Error')
         //console.log(error.response)
         l.then(() => message.error('Error', 2.5))
        })
    }
       //submit form
       handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            let date  = values.birthYear._d.toLocaleDateString().split('/')
            var form = new FormData();
            
            if(this.state.file)
            form.append('img',this.state.file);

            /*if(this.state.file2)
            for(var i=0 ; i<= this.state.file2.length-1 ; i++)
            {
                form.append(`transportLicense`,this.state.file2[i] )
            }

            if(this.state.file3)
            for(var i=0 ; i<= this.state.file3.length-1 ; i++)
            {
                form.append(`transportImages`,this.state.file3[i] )
            }*/


            //form.append('transportLicense',this.state.file2);
            //form.append('transportImages',this.state.file3);
            form.append('username',values.username);
            form.append('email',values.email);
            form.append('phone',values.phone);
            form.append('gender',values.gender.key);
            form.append('birthYear',date[2]);
            form.append('type','SALES-MAN');
            form.append('transportType',values.transportType);
            form.append('manufacturingYear',values.manufacturingYear);
            form.append('bank',values.bank);
            form.append('bankNumber',values.bankNumber);
            form.append('signUpFrom',values.signUpFrom);
            form.append('balance', values.balance);
            form.append('rate', values.rate);
            form.append('ratePercent', values.ratePercent);

            let l = message.loading(allStrings.wait, 2.5)
            axios.put(`${BASE_END_POINT}user/${this.state.salesMan.id}/updateInfo`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.updatedDone, 2.5));
                this.setState({ modal1Visible:false });
                this.props.history.goBack()
            })
            .catch(error=>{
             //   console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })

          }
        });
        
      }

    //end submit form
      
      //modal
     
  
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
  

  //end modal
      
    render() {
        const {salesMan} = this.state
        console.log("Sales man info   ",salesMan)
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
         const {select} = this.props;
      return (
          
        <div>
         <AppMenu height={'270%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
        <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff'}}>Sales Man Info</h2>
                </div>
                <div class="row">
                
                    <form class="col s12">
                    <img style={{borderColor:'#25272e'}} src={ 'img' in salesMan?salesMan.img:"https://theimag.org/wp-content/uploads/2015/01/user-icon-png-person-user-profile-icon-20.png"}></img>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="firstname" type="text" class="validate" disabled value={salesMan.username}>
                            </input>
                            <label for="firstname" class="active">{allStrings.name}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="lastname" type="text" class="validate" disabled value={salesMan.phone}></input>
                            <label for="lastname" class="active">{allStrings.phone}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="email" type="text" class="validate" disabled value={salesMan.email}>
                            </input>
                            <label for="email" class="active">{allStrings.email}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="phone" type="text" class="validate" disabled value={salesMan.signUpFrom}></input>
                            <label for="phone" class="active">{allStrings.signUpFrom}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="type" type="text" class="validate" disabled value={salesMan.gender}>
                            </input>
                            <label for="type" class="active">{allStrings.gender}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="are" type="text" class="validate" disabled value={salesMan.birthYear}></input>
                            <label for="area" class="active">{allStrings.birthYear}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={salesMan.rate}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.rate}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="Address" type="text" class="validate" disabled value={salesMan.ratePercent}></input>
                            <label for="Address" class="active">{allStrings.ratePercent}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={salesMan.tasksCount}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.taskCount}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="Address" type="text" class="validate" disabled value={salesMan.hasCoupon}></input>
                            <label for="Address" class="active">{allStrings.hasCopon}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={salesMan.balance}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.balance}</label>
                            </div>
                           
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={salesMan.active}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.active}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="Address" type="text" class="validate" disabled value={salesMan.id}></input>
                            <label for="Address" class="active">{allStrings.id}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="Job" type="text" class="validate" disabled value={salesMan.bank}>
                            </input>
                            <label for="Job" class="active">{allStrings.bank}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="job Location" type="text" class="validate" disabled value={salesMan.bankNumber}></input>
                            <label for="job Location" class="active">{allStrings.bankNumber}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="Job" type="text" class="validate" disabled value={salesMan.manufacturingYear}>
                            </input>
                            <label for="Job" class="active">{allStrings.manufacturingYear}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="job Location" type="text" class="validate" disabled value={salesMan.transportType}></input>
                            <label for="job Location" class="active">{allStrings.transportType}</label>
                            </div>
                        </div>

                        <h6 style={{display:'flex',justifyContent:'flex-start',  marginTop:25}} >transport License</h6>
                        <div class="row">
                            {this.state.transportLicense.map(img=>(
                            <img style={{width:150,height:150,borderRadius:10,border:'1px solid black',marginLeft:20,marginRight:20}} src={img} />

                            ))}
                        </div>

                        <h6 style={{display:'flex',justifyContent:'flex-start',marginTop:25}} >transport Images</h6>
                        <div class="row">
                            {this.state.transportImages.map(img=>(
                            <img style={{width:150,height:150,borderRadius:10,border:'1px solid black',marginLeft:20,marginRight:20}} src={img} />

                            ))}
                        </div>
                        <a class="waves-effect waves-light btn btn-large delete"  onClick={()=>this.deleteUser()} ><i class="spcial material-icons left">delete</i>{allStrings.remove}</a>
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.activeUser(false)}><i class="material-icons left spcial">block</i>{allStrings.block}</a>
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.activeUser(true)}><i class="material-icons left spcial">remove</i>{allStrings.unblock}</a>                 
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class=" spcial material-icons left">edit</i>Edit</a>
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setState({ordermodal:true}) }>{allStrings.order}</a>
                        </form>
                    <Modal
                            title="Edit"
                            visible={this.state.modal1Visible}
                            onOk={this.handleSubmit}
                            onCancel={() => this.setModal1Visible(false)}
                        >
                        
                            <Form onSubmit={this.handleSubmit} className="login-form">
                            
                            <label for="name" class="lab">{allStrings.name}</label>
                            <Form.Item>
                            {getFieldDecorator('username', {
                                rules: [{ required: true, message: 'Please enter  Name' }],
                                initialValue:salesMan.username
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.email}</label>
                            <Form.Item>
                            {getFieldDecorator('email', {
                                rules: [{ required: true, message: 'Please enter  email' }],
                                initialValue:salesMan.email
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.phone}</label>
                            <Form.Item>
                            {getFieldDecorator('phone', {
                                rules: [{ required: true, message: 'Please enter  phone' }],
                                initialValue:salesMan.phone
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.gender}</label>
                            <Form.Item>
                            {getFieldDecorator('gender', {
                                rules: [{ required: true, message: 'Please enter gender' }],
                            })(
                                <Select labelInValue  
                                placeholder={allStrings.gender}
                                style={{ width: '100%'}} onChange={handleChange}>
                                    <Option value="male">{allStrings.male}</Option>
                                    <Option value="female">{allStrings.female}</Option>
                                </Select>
                            )}
                            </Form.Item>


                            <label for="name" class="lab">{allStrings.birthYear}</label>
                            <Form.Item>
                            {getFieldDecorator('birthYear', {
                                rules: [{ required: true, message: 'Please enter birth Year' }],
                            })(
                            <DatePicker style={{ width: '100%'}}/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.signUpFrom}</label>
                            <Form.Item>
                            {getFieldDecorator('signUpFrom', {
                                rules: [{ required: true, message: 'Please enter  signUp From' }],
                                initialValue:salesMan.signUpFrom
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.signUpFrom}</label>
                            <Form.Item>
                            {getFieldDecorator('transportType', {
                                rules: [{ required: true, message: 'Please enter  transportType' }],
                                initialValue:salesMan.transportType
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.manufacturingYear}</label>
                            <Form.Item>
                            {getFieldDecorator('manufacturingYear', {
                                rules: [{ required: true, message: 'Please enter  manufacturing Year' }],
                                initialValue:salesMan.manufacturingYear
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.bank}</label>
                            <Form.Item>
                            {getFieldDecorator('bank', {
                                rules: [{ required: true, message: 'Please enter  bank' }],
                                initialValue:salesMan.bank
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.manufacturingYear}</label>
                            <Form.Item>
                            {getFieldDecorator('bankNumber', {
                                rules: [{ required: true, message: 'Please enter bankNumber' }],
                                initialValue:salesMan.bankNumber
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                
                           
                            
                            <label for="name" class="lab">{allStrings.balance}</label>
                            <Form.Item>
                            {getFieldDecorator('balance', {
                                rules: [{ required: true, message: 'Please enter balacne' }],
                                initialValue:salesMan.balance
                            })(
                                <Input />
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.rate}</label>
                            <Form.Item>
                            {getFieldDecorator('rate', {
                                rules: [{ required: true, message: 'Please enter rate' }],
                                initialValue:salesMan.rate
                            })(
                                <Input />
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.ratePercent}</label>
                            <Form.Item>
                            {getFieldDecorator('ratePercent', {
                                rules: [{ required: true, message: 'Please enter rate percent' }],
                                initialValue:salesMan.ratePercent
                            })(
                                <Input />
                            )}
                            </Form.Item>
                           
                            </Form>

                            <label> اضف صورتك الشخصية </label> <br></br>
                            <input type="file" onChange= {this.onChange} multiple></input>

                            <div style={{marginTop:20}}>
                            <label> اضف صور من رخصة سيارتك </label>
                            <div>
                            <input   type="file" onChange= {this.onChange2} multiple></input>
                            </div>
                            </div>


                            <div style={{marginTop:20}}>
                            <label> اضف صور لسيارتك </label>
                            <div>
                            <input  type="file" onChange= {this.onChange3} multiple></input>
                            </div>
                            </div>
                            
                   
                        </Modal>
                        <Modal
                            title={allStrings.orders}
                            visible={this.state.ordermodal}                          
                            okText={allStrings.ok}
                            cancelText={allStrings.cancel}
                            onCancel={() => this.setState({ordermodal:false})}
                        >

                          

                          <div className='orderTypes'>
                            <button onClick={()=>{
                              this.getOrders(1,'PENDING',true)
                              this.page=1;
                              this.type='PENDING'
                            }}
                            >{allStrings.pendingOrder}</button>

                            <button onClick={()=>{
                              this.getOrders(1,'CANCEL',true)
                              this.page=1;
                              this.type='CANCEL'
                            }}
                            >{allStrings.canceledOrder}</button>

                            <button
                            onClick={()=>{
                              this.getOrders(1,'ON_PROGRESS',true)
                              this.page=1;
                              this.type='ON_PROGRESS'
                            }}
                            >{allStrings.onprogressOrder}</button>

                            <button
                            onClick={()=>{
                              this.getOrders(1,'DELIVERED',true)
                              this.page=1;
                              this.type='DELIVERED'
                            }}
                            >{allStrings.deliveredOrder}</button>

                            

                          </div>
                          {this.state.selectFlag?
                          <table>
                            <tr  className='tableHeader'>
                              <th>{allStrings.name}</th>
                              <th>{allStrings.phone}</th>
                              <th>{allStrings.order}</th>
                              <th>{allStrings.time}</th>
                              <th>{allStrings.shopName}</th>
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{allStrings.salesmanName}</th>
                              }
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{allStrings.salesmanPhone}</th>
                              }
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{allStrings.offerPrice}</th>
                              }
                            </tr>
                            
                            {this.state.orders.map(order=>
                            <tr onClick={()=>{
                              this.props.history.push('/OrderInfo',{data:order})
                            }} className='tableContent'>
                              <th>{order.client.username}</th>
                              <th>{order.client.phone}</th>
                              <th>{order.description}</th>
                              <th>{order.time}</th>
                              <th>{order.shopName}</th>
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{order.salesMan.username}</th>
                              }
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{order.salesMan.phone}</th>
                              }
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{order.offer.deliveryCost}</th>
                              }
                            </tr>
                            )}
                            
                          </table>
                          :
                          <div className='selectType'> 
                          <span >{allStrings.selectType}</span>
                          </div>
                          }
                          {this.state.selectFlag&& 
                          <div className='orderLoadMore'>
                            <button onClick={()=>{
                              this.getOrders(this.page+1);
                              this.page++;
                            }}>{allStrings.loadMore}</button>
                          </div>
                          }
                          </Modal>     
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps) (SalesManInfo = Form.create({ name: 'normal_login' })(SalesManInfo));
