import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './salesMan info.css';
import {Input} from 'react-materialize';
import "antd/dist/antd.css";
 import { Modal,Form ,Select ,message,DatePicker} from 'antd';
 import axios from 'axios';
 import {BASE_END_POINT} from '../../config/URL'
import {Table} from 'react-materialize'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'



class SalesManInfoNoti extends React.Component {
  page=1;
  type=null;

      state = {
        modal1Visible: false,
        salesMan:this.props.location.state.data,
        file:null,
        file2:null,
        file3:null,
        ordermodal:false,
        orders:[],
        loading:false,
        selectFlag:false,
        transportImages:this.props.location.state.data.transportImages.includes(",")? this.props.location.state.data.transportImages.split(','):[this.props.location.state.data.transportImages],
        transportLicense:this.props.location.state.data.transportLicense.includes(",")?this.props.location.state.data.transportLicense.split(','):[this.props.location.state.data.transportLicense]

      }

      constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }



    activeUser = (active) => {
        let uri ='';
        if(active){
         uri = `${BASE_END_POINT}${this.state.salesMan.id}/active`
        }else{
         uri = `${BASE_END_POINT}${this.state.salesMan.id}/disactive`
        }
       let l = message
        .loading(allStrings.wait, 2.5)
        axios.put(uri,{},{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
        .then(response=>{
            // console.log('done')
             if(active){
                 
                 l.then(() => message.success(allStrings.addDone, 2.5))
                 
             }else{
             
                l.then(() => message.success(allStrings.blockDone, 2.5))
             }
             this.props.history.goBack()
        })
        .catch(error=>{
        // console.log('Error')
         //console.log(error.response)
         l.then(() => message.error('Error', 2.5))
        })
    }
   

  
      
    render() {
        const {salesMan} = this.state
        console.log("Sales man info   ",salesMan)
        
         const {select} = this.props;
      return (
          
        <div>
         <AppMenu height={'270%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
        <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff'}}>Sales Man Info</h2>
                </div>
                <div class="row">
                
                    <form class="col s12">
                    <img style={{borderColor:'#25272e'}} src={ 'img' in salesMan?salesMan.img:"https://theimag.org/wp-content/uploads/2015/01/user-icon-png-person-user-profile-icon-20.png"}></img>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="firstname" type="text" class="validate" disabled value={salesMan.username}>
                            </input>
                            <label for="firstname" class="active">{allStrings.name}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="lastname" type="text" class="validate" disabled value={salesMan.phone}></input>
                            <label for="lastname" class="active">{allStrings.phone}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="email" type="text" class="validate" disabled value={salesMan.email}>
                            </input>
                            <label for="email" class="active">{allStrings.email}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="phone" type="text" class="validate" disabled value={salesMan.signUpFrom}></input>
                            <label for="phone" class="active">{allStrings.signUpFrom}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="type" type="text" class="validate" disabled value={salesMan.gender}>
                            </input>
                            <label for="type" class="active">{allStrings.gender}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="are" type="text" class="validate" disabled value={salesMan.birthYear}></input>
                            <label for="area" class="active">{allStrings.birthYear}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={salesMan.rate}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.rate}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="Address" type="text" class="validate" disabled value={salesMan.ratePercent}></input>
                            <label for="Address" class="active">{allStrings.ratePercent}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={salesMan.tasksCount}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.taskCount}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="Address" type="text" class="validate" disabled value={salesMan.hasCoupon}></input>
                            <label for="Address" class="active">{allStrings.hasCopon}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={salesMan.balance}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.balance}</label>
                            </div>
                           
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={salesMan.active}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.active}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="Address" type="text" class="validate" disabled value={salesMan.id}></input>
                            <label for="Address" class="active">{allStrings.id}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="Job" type="text" class="validate" disabled value={salesMan.bank}>
                            </input>
                            <label for="Job" class="active">{allStrings.bank}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="job Location" type="text" class="validate" disabled value={salesMan.bankNumber}></input>
                            <label for="job Location" class="active">{allStrings.bankNumber}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="Job" type="text" class="validate" disabled value={salesMan.manufacturingYear}>
                            </input>
                            <label for="Job" class="active">{allStrings.manufacturingYear}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="job Location" type="text" class="validate" disabled value={salesMan.transportType}></input>
                            <label for="job Location" class="active">{allStrings.transportType}</label>
                            </div>
                        </div>

                        <h6 style={{display:'flex',justifyContent:'flex-start',  marginTop:25}} >transport License</h6>
                        <div class="row">
                            {this.state.transportLicense.map(img=>(
                            <img style={{width:150,height:150,borderRadius:10,border:'1px solid black',marginLeft:20,marginRight:20}} src={img} />

                            ))}
                        </div>

                        <h6 style={{display:'flex',justifyContent:'flex-start',marginTop:25}} >transport Images</h6>
                        <div class="row">
                            {this.state.transportImages.map(img=>(
                            <img style={{width:150,height:150,borderRadius:10,border:'1px solid black',marginLeft:20,marginRight:20}} src={img} />

                            ))}
                            
                        </div>
                        {salesMan.active?
                        <span>{allStrings.salesmanadded}</span>
                        :
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.activeUser(true)}>{allStrings.agree}</a>
                        }
                        </form>

                    
                    </div>
            </div>
        </div>
        </div>
       
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps) (SalesManInfoNoti = Form.create({ name: 'normal_login' })(SalesManInfoNoti));
