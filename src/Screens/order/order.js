import React from 'react';
import Menu from '../../components/menu/menu';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './order.css';
import {Skeleton, Icon,Popconfirm, message} from 'antd';
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'

class Pending extends React.Component {
  pagentationPage=0;
    counter=0;
    state = {
        visible: false,
        confirmDelete: false,
        selectedOrder:null,
        visible: false,
        orders:[],
        loading:true,
        tablePage:0,
        }

        constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
       //submit form
       //PENDING
       flag = -1;
       getOrders= (page,deleteRow) => {
         axios.get(`${BASE_END_POINT}orders?page=${page}&limit={20}`)
         .then(response=>{
           console.log("ALL orders")
           console.log(response.data.data)
           console.log(response.data.data[0].productOrders)
           if(deleteRow){
            this.setState({tablePage:0})
           }
           this.setState({orders:deleteRow?response.data.data:[...this.state.orders,...response.data.data],loading:false})
         })
         .catch(error=>{
           console.log("ALL orders ERROR")
           console.log(error.response)
           this.setState({loading:false})
         })
       }
       componentDidMount(){
         console.log(this.props.currentUser)
         this.getOrders(1,true)
       }
       OKBUTTON = (e) => {
        this.deleteOrder()
       }
 
       deleteOrder = () => {
         let l = message.loading(allStrings.wait, 2.5)
         axios.delete(`${BASE_END_POINT}orders/${this.state.selectedOrder}`,{
           headers: {
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
         .then(response=>{
             l.then(() => message.success(allStrings.deleteDone, 2.5))
             this.getOrders(1,true)
                this.flag = -1
         })
         .catch(error=>{
             console.log(error.response)
             l.then(() => message.error('Error', 2.5))
         })
      }

    render() {
 
      let controls = (
        <Popconfirm
        title={allStrings.areYouSure}
        onConfirm={this.OKBUTTON}
        onCancel={this.fCANCELBUTTON}
        okText={allStrings.yes}
        cancelText={allStrings.no}
      >
         <Icon className='controller-icon' type="delete" />
      </Popconfirm>
     )
  let list = this.state.orders.map((val,index)=>{
        
    return  [
    val.id,val.shopName,val.description,val.client.username,val.client.phone,
    val.createdAt.substring(0, 10),val.status
    ]
  })    

  list.forEach(function(row) {
    row.push(controls)
   });

   const loadingView = [
    [<Skeleton  active/> ],
    [<Skeleton active/> ],
    [<Skeleton  active/> ],
    [<Skeleton active/> ],
    
   ]

   const {select} = this.props;
 
      return (
        <div>
        <AppMenu height={'140%'} goTo={this.props.history} />
        <Nav></Nav>
        <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
        <Tables columns={this.state.loading?['wait']:[allStrings.id,allStrings.shopName,allStrings.order,allStrings.client,allStrings.phone,allStrings.date,allStrings.status, allStrings.remove]} 
        title={allStrings.pendingtable}
        page={this.state.tablePage}
        onCellClick={(colData,cellMeta,)=>{
          console.log('col index  '+cellMeta.colIndex)
          console.log('row index   '+colData)
          if(cellMeta.colIndex!=7){
            
            console.log(this.state.orders[cellMeta.rowIndex])
            this.props.history.push('/OrderInfo',{data:this.state.orders[this.pagentationPage+cellMeta.rowIndex]})
          }else if(cellMeta.colIndex==7){
              const id = list[this.pagentationPage+cellMeta.rowIndex][0];
              this.setState({selectedOrder:id})
              console.log(id)
            }
        }}
         onChangePage={(currentPage)=>{
           this.setState({tablePage:currentPage})
          if(currentPage>this.counter){
            this.counter=currentPage;
            this.pagentationPage=this.pagentationPage+10
          }else{
           this.counter=currentPage;
           this.pagentationPage=this.pagentationPage-10
          }
                console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getOrders(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
        arr={this.state.loading?loadingView:list}
       
        ></Tables>
        <Footer></Footer>
        </div>

    </div>
      );
    }
  }

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }


export default withRouter(connect(mapToStateProps,mapDispatchToProps) (Pending) );
