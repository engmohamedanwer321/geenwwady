import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './order info.css';
import "antd/dist/antd.css";
 import { Modal,Form ,Select ,DatePicker,message,Input} from 'antd';
 import axios from 'axios';
 import moment from 'moment'
import {BASE_END_POINT} from '../../config/URL'
import {Table} from 'react-materialize'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class OrderInfo extends React.Component {
  page =1;
    state = {
        visible: false,
        order:this.props.location.state.data,
        salesman:[],
        branches:[],
        chatModel:false,
        messages:[],
    }

    constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }

        //status: "ACCEPTED"
        console.log("Order info  ",this.props.location.state.data)
      }

      getMessages(page) {
        const {order} = this.state;
        this.setState({loading:true})
        if(order.accept){
          // userId=4&&friendId=2 
        axios.get(`${BASE_END_POINT}messages?userId=${order.clent.id}&&friendId=${order.salesMan.id}&&orderId=${order.id}&&page=${page}&&limit=20`)
        .then(response => {
            console.log("MEssages")
            console.log(response.data)
           
            this.setState({loading:false,messages:[...this.state.messages,...response.data.data.reverse()]})
        }).catch(error => {
            this.setState({loading:false})
            console.log("message error");
            console.log(error.response);
            console.log(error);
            
        })
        }
  }

    getSalesman = () => {
        axios.get(`${BASE_END_POINT}find?type=SALES-MAN`)
        .then(response=>{
          console.log("ALL SalesMAn")
          console.log(response.data)
          this.setState({salesman:response.data.data})
        })
        .catch(error=>{
          console.log("ALL SalesMAn ERROR")
          console.log(error.response)
        })
      }
    getBranch = () => {
        axios.get(`${BASE_END_POINT}branch`)
        .then(response=>{
          console.log("ALL branches")
          console.log(response.data)
          this.setState({branches:response.data.data})
        })
        .catch(error=>{
          console.log("ALL branches ERROR")
          console.log(error.response)
        })
      }


    componentDidMount(){
        if(this.state.order.status!='PENDING'&&this.state.order.status!='CANCEL'){
          this.getMessages(this.page)
        }
        
        console.log("order    ", this.state.order)
     
        this.getSalesman()
        this.getBranch()
    }
    deleteOrder = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}/orders/${this.state.order.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            /*if(this.order.status === "PENDING"){
                this.props.history.push('/pendingOrder');
            }*/
            this.props.history.goBack()
           

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }
    //submit form
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            const data = {
                status:values.status.key
            }
            if(values.salesMan){
                data.salesMan = values.salesMan.key
            }
            if(values.branch){
                data.branch = values.branch.key
            }
            if(values.paidDate){
                data.paidDate = values.paidDate._d.toLocaleDateString()         
                console.log(values.paidDate._d.toLocaleDateString())          
            }

            if(values.reason){
                data.reason = values.reason
            }

            console.log(data)
            
            let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}orders/${this.state.order.id}/users/${this.state.order.client.id}/status`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.setState({ visible:false });
              this.props.history.goBack()
          })
          .catch(error=>{
              console.log(error.response)
              l.then(() => message.error('Error', 2.5))
          })
          
          }
        });
        
      }
    //end submit form
  

    showModal = () => {
      this.setState({
        visible: true,
      });
    }
  
    handleOk = (e) => {
      console.log(e);
      this.setState({
        visible: false,
      });
    }
  
    handleCancel = (e) => {
      console.log(e);
      this.setState({
        visible: false,
      });
    }
  
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {order} = this.state

         function handleChange(value) {
            console.log(value); 
         }
         //end select
         const {select} = this.props;
      return (
          
        <div>
          <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#25272e'}} >
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.orderInfo}</h2>
                </div>
                <div class="row">
                    <form class="col s12">


                        <div className='dash-table'>
                            <h5>{allStrings.client} :</h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="name">{allStrings.name}</th>
                                            <th data-field="email">{allStrings.email}</th>
                                            <th data-field="mobile">{allStrings.phone}</th>
                                            <th data-field="address">{allStrings.couponNum}</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr onClick={()=>{
                                                this.props.history.push('/UserInfo',{data:this.state.order.client})
                                                }}>
                                                <td>{order.client.id}</td>
                                                <td>{order.client.username}</td>
                                                <td>{order.client.email}</td>
                                                <td>{order.client.phone}</td>
                                                <td>{order.client.coupon}</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div>

                        <div className='dash-table'>
                            <h5>{allStrings.order} :</h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="id">{allStrings.order}</th>
                                            <th data-field="name">{allStrings.shopName}</th>
                                            <th data-field="name">{allStrings.status}</th>
                                            { 'salesMan' in order && 
                                            <th data-field="mobile">{allStrings.offerPrice}</th>
                                            }                                                                            
                                            </tr>
                                        </thead>

                                        <tbody>                                          
                                            <tr>
                                            <td>{order.id}</td>
                                            <td>{order.description}</td>
                                            <td>{order.shopName}</td>
                                            <td>{order.status}</td> 
                                            { 'salesMan' in order && 
                                             <td>{order.offer.deliveryCost}</td>
                                            }                                 
                                            </tr>                                                                                  
                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div>
                       
                        {  'salesMan' in order &&
                          <div className='dash-table'>
                          <h5>{allStrings.salesman} :</h5>
                          <div className='row'>
                              <div className="col s6 m6 l6 dashboard-table">
                                  <Table>
                                      <thead>
                                          <tr>
                                          <th data-field="id">{allStrings.id}</th>
                                          <th data-field="name">{allStrings.name}</th>
                                          <th data-field="email">{allStrings.email}</th>
                                          <th data-field="mobile">{allStrings.phone}</th>
                                      
                                          
                                          
                                          </tr>
                                      </thead>

                                      <tbody>
                                          <tr onClick={()=>{
                                              this.props.history.push('/SalesManInfo',{data:this.state.order.salesMan})
                                              }}>
                                              <td>{order.salesMan.id}</td>
                                              <td>{order.salesMan.username}</td>
                                              <td>{order.salesMan.email}</td>
                                              <td>{order.salesMan.phone}</td>
                                             
                                              
                                            
                                          </tr>
                                      </tbody>
                                  </Table>
                              </div>
                          </div>
                      </div>
                        }
                        <a class="waves-effect waves-light btn btn-large delete" onClick={this.deleteOrder}><i class="spcial material-icons left">delete</i>{allStrings.remove}</a>
                        {
                          this.state.order.accept &&
                          <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setState({chatModel:true}) }>{allStrings.chat}</a>
                        }
             
                            <div>
                                <Modal
                                title={allStrings.changestatus}
                                visible={this.state.visible}
                                onOk={this.handleSubmit}
                                onCancel={this.handleCancel}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                >
                                <Form onSubmit={this.handleSubmit} className="login-form">
                    
                                    <Form.Item>
                                        {getFieldDecorator('status', {
                                            rules: [{ required: true, message: 'Please enter status' }],
                                        })(
                                            <Select labelInValue defaultValue={{ key: 'ACCEPTED' }} 
                                            placeholder={allStrings.status}
                                            style={{ width: '100%'}} onChange={handleChange}>
                                                {
                                                   this.props.location.state.data.status=='PENDING'&&
                                                   <Option value="ACCEPTED">{allStrings.accepted}</Option>
                                                }
                                                
                                                {
                                                  this.props.location.state.data.status=='PENDING'&&
                                                  <Option value="REFUSED">{allStrings.refused}</Option>
                                                }
                                                
                                                {
                                                   this.props.location.state.data.status=='ACCEPTED'&&
                                                   <Option value="ON_THE_WAY">{allStrings.ontheway}</Option>
                                                }

                                                {
                                                   this.props.location.state.data.status=='ACCEPTED'|| this.props.location.state.data.status=='ON_THE_WAY'&&
                                                   <Option value="DELIVERED">{allStrings.deliverd}</Option>
                                                }
                                                
                                                
                                            </Select>
                                        )}
                                        </Form.Item>
                                        
                                        {order.status == 'PENDING'&&
                                        <Form.Item>
                                        {getFieldDecorator('paidDate', {
                                            rules: [{ required: false, message: 'Please enter Paid Date' }],
                                        })(
                                            <DatePicker style={{ width: '100%'}}/>
                                        )}
                                        
                                        </Form.Item>
                                        }

                                        {order.status == 'PENDING'&&
                                        <Form.Item>
                                        {getFieldDecorator('salesMan', {
                                            rules: [{ required: false, message: 'Please enter sales Man' }],
                                        })(
                                            <Select labelInValue defaultValue={{ key: '1' }} 
                                            placeholder={allStrings.salesman}
                                            style={{ width: '100%'}} onChange={handleChange}>
                                                {this.state.salesman.map(item=>
                                                  <Option value={item.id}>{item.firstname +" "+ item.lastname}</Option>
                                                )}
                                           
                                            </Select>
                                        )}
                                        </Form.Item>
                                        }
                                        {order.status == 'PENDING'&&
                                        <Form.Item>
                                        {getFieldDecorator('branch', {
                                            rules: [{ required: false, message: 'Please enter branch' }],
                                        })(
                                            <Select labelInValue defaultValue={{ key: '1' }} 
                                            placeholder={allStrings.branches}
                                            style={{ width: '100%'}} onChange={handleChange}>
                                                {this.state.branches.map(item=>
                                                  <Option value={item.id}>{item.branchName}</Option>
                                                )}
                                           
                                            </Select>
                                        )}
                                        </Form.Item>
                                        }

                                        {order.status == 'PENDING'&&
                                        <Form.Item>
                                            {getFieldDecorator('reason', {
                                                rules: [{ required: false, message: 'Please enter description' }],
                                            })(
                                                <Input placeholder={allStrings.reason} />
                                            )}
                                            </Form.Item>
                                        }
                                </Form>
                                </Modal>
                            </div>
                        </form>
                        
                </div>
               
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
              <Modal
                    title={allStrings.chat}
                    visible={this.state.chatModel}
                    onOk={()=>{this.setState({chatModel:false})}}
                    onCancel={()=>{this.setState({chatModel:false})}}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                  <div class="chat"> 
                    
                      
                      {this.state.messages.map(msg=>(
                        msg.user._id!=this.state.order.client.id?
                        <div class="chat-message me">{msg.text}
                          <span class="distext">{moment(msg.createdAt).fromNow()}</span>
                          <br />
                          {msg.image&&
                          <img style={{width:200,height:300}} src={msg.image} alt="Logo" />
                          }
                          
                        </div>
                        :
                        <div class="chat-message them">{msg.text}
                          <span class="distextright">{moment(msg.createdAt).fromNow()}</span>
                          <br />
                          {msg.image&&
                          <img style={{width:200,height:300}} src={msg.image} alt="Logo" />
                          }
                        
                      </div>
                      ))}
                      
                 </div>
                
                {this.state.order.status!='PENDING'&&this.state.order.status!='CANCEL'&&this.state.messages.length>0&& 
                 <a onClick={()=>{
                  this.page++
                  this.getMessages(this.page)
                }} style={{display:"block",color:"#26a69a",textAlign:"center",marginBottom:" 25px",marginTop:50}}> + load more</a>     
                }
                </Modal>
                        
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default withRouter( connect(mapToStateProps,mapDispatchToProps) (OrderInfo = Form.create({ name: 'normal_login' })(OrderInfo)));
