import React, { Component } from 'react';
import Menu from '../../components/menu/menu';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './user info.css';
import {Icon,Button,Card,Input,CardTitle} from 'react-materialize';
import "antd/dist/antd.css";
 import { Modal,Form ,Select ,DatePicker,message} from 'antd';
 import axios from 'axios';
 import {BASE_END_POINT} from '../../config/URL'
import {Table} from 'react-materialize'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
import {getUser} from '../../actions/AuthActions'



class UserInfo extends React.Component {
       //submit form
       page=1;
       type=null;
       state = {
        modal1Visible: false,
        user:this.props.location.state.data,
        file:null,
        ordermodal:false,
        orders:[],
        loading:false,
        selectFlag:false,
        //this.props.location.state.data.img[0],
        
      }

      constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }

      getOrders= (page,type,reload) => {
        console.log("page  ",page)
        axios.get(`${BASE_END_POINT}orders?client=${this.state.user.id}&status=${type}&page=${page}&limit={20}`)
        .then(response=>{
          console.log("ALL orders")
          console.log(response.data.data)
          this.setState({selectFlag:true, orders:reload?response.data.data : [...this.state.orders,...response.data.data],loading:false})
        })
        .catch(error=>{
          console.log("ALL orders ERROR")
          console.log(error.response)
          this.setState({loading:false})
        })
      }

      onChange = (e) => {
        this.setState({file:e.target.files[0]});
    }

       componentDidMount()
       {
           //this.getOrders(this.page)
       }

       deleteUser = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}${this.state.user.id}/delete`,{
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()
        })
        .catch(error=>{
            //console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }


       block = (active) => {
           let uri ='';
           if(active){
            uri = `${BASE_END_POINT}${this.state.user.id}/block`
           }else{
            uri = `${BASE_END_POINT}${this.state.user.id}/unblock`
           }
          let l = message.loading(allStrings.wait, 2.5)
           axios.put(uri,{},{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
           .then(response=>{
               // console.log('done')
                if(active){
                    
                    l.then(() => message.success(allStrings.unblockDone, 2.5))
                    
                }else{
                
                   l.then(() => message.success(allStrings.blockDone, 2.5))
                }
                this.props.history.goBack()
           })
           .catch(error=>{
           // console.log('Error')
           // console.log(error.response)
            l.then(() => message.error('Error', 2.5))
           })
       }

     

       handleSubmit = (e) => {
        e.preventDefault();
        console.log('user ID     ',this.state.user.id)
        this.props.form.validateFields((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            var form = new FormData();
            if(this.state.file){
                form.append('img',this.state.file);
            }
            form.append('username', values.username);
            form.append('email', values.email);
            form.append('phone', values.phone);
            //form.append('type', 'ADMIN');
            form.append('gender', values.gender.key);
            form.append('birthYear', values.birthYear);
            form.append('balance', values.balance);
            form.append('rate', values.rate);
            form.append('ratePercent', values.ratePercent);
            let l = message.loading(allStrings.wait, 2.5)
            axios.put(`${BASE_END_POINT}user/${this.state.user.id}/updateInfo`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.updatedDone, 2.5));
                const user = {...this.props.currentUser,user:{...response.data.user}}
                //localStorage.setItem('test', JSON.stringify(user));
                localStorage.setItem('@QsathaUser', JSON.stringify(user));  
                this.props.getUser(user);
                console.log("update      ",response.data)
                this.setState({ modal1Visible:false });
                this.props.history.goBack()
            })
            .catch(error=>{
                console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })
          }
        });
       
        
      }

    //end submit form
      
      //modal
    
  
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
  

  //end modal
  
    render() {
        const { getFieldDecorator } = this.props.form;
        const {user} = this.state;
         //select
         const Option = Select.Option;

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
         const {select} = this.props;
 
      return (
          
        <div>
         <AppMenu height={'200%'} goTo={this.props.history} />
        <Nav></Nav>
        <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}}>
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.userinfo}</h2>
                </div>
                <div class="row">
                
                    <form class="col s12">
                    <img style={{borderColor:'#25272e'}} src={ 'img' in user?user.img:"https://theimag.org/wp-content/uploads/2015/01/user-icon-png-person-user-profile-icon-20.png"}></img>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="firstname" type="text" class="validate" disabled value={user.username}>
                            </input>
                            <label for="firstname" class="active">{allStrings.name}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="email" type="text" class="validate" disabled value={user.phone}>
                            </input>
                            <label for="email" class="active">{allStrings.phone}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="email" type="text" class="validate" disabled value={user.signUpFrom}>
                            </input>
                            <label for="email" class="active">{allStrings.signUpFrom}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="phone" type="text" class="validate" disabled value={user.email}></input>
                            <label for="phone" class="active">{allStrings.email}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.active}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.active}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.id}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.id}</label>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="type" type="text" class="validate" disabled value={user.gender}>
                            </input>
                            <label for="type" class="active">{allStrings.gender}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="are" type="text" class="validate" disabled value={user.birthYear}></input>
                            <label for="area" class="active">{allStrings.birthYear}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.rate}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.rate}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="Address" type="text" class="validate" disabled value={user.ratePercent}></input>
                            <label for="Address" class="active">{allStrings.ratePercent}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.tasksCount}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.taskCount}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="Address" type="text" class="validate" disabled value={user.hasCoupon}></input>
                            <label for="Address" class="active">{allStrings.hasCopon}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.balance}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.balance}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.block}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.block}</label>
                            </div>
                           
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.rate}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.rate}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={`${user.ratePercent} %`}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.ratePercent}</label>
                            </div>
                           
                        </div>

                        {this.state.user.type!='ADMIN'&&            
                        <div>
                        <a class="waves-effect waves-light btn btn-large delete"  onClick={()=>this.deleteUser()} ><i class="spcial material-icons left">delete</i>{allStrings.remove}</a>
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.block(true)}><i class="material-icons left spcial">block</i>{allStrings.block}</a>
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.block(false)}><i class="material-icons left spcial">remove</i>{allStrings.unblock}</a>                 
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class=" spcial material-icons left">edit</i>{allStrings.edit}</a>
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setState({ordermodal:true}) }>{allStrings.order}</a>
                        </div>
                        }
                                                                             
                        </form>
                    <Modal
                            title="Edit"
                            visible={this.state.modal1Visible}
                            onOk={this.handleSubmit}
                            okText={allStrings.ok}
                            cancelText={allStrings.cancel}
                            onCancel={() => this.setModal1Visible(false)}
                        >
                            
                            <Form onSubmit={this.handleSubmit} className="login-form">

                             <label for="name" class="lab">{allStrings.name}</label>
                            <Form.Item>
                            {getFieldDecorator('username', {
                                rules: [{ required: true, message: 'Please enter username' }],
                                initialValue: user.username,
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.email}</label>
                            <Form.Item>
                            {getFieldDecorator('email', {
                                rules: [{ required: true, message: 'Please enter email' }],
                                initialValue:user.email
                            })(
                                <Input />
                            )}
                            </Form.Item>
                        
                            <label for="name" class="lab">{allStrings.phone}</label>
                            <Form.Item>
                            {getFieldDecorator('phone', {
                                rules: [{ required: true, message: 'Please enter phone' }],
                                initialValue:user.phone
                            })(
                                <Input/>
                            )}
                            </Form.Item>


                            <label for="name" class="lab">{allStrings.birthYear}</label>
                            <Form.Item>
                            {getFieldDecorator('birthYear', {
                                rules: [{ required: true, message: 'Please enter birthYear' }],
                                initialValue:user.birthYear
                            })(
                                <Input/>
                            )}
                            </Form.Item>
                            
                            <Form.Item>
                            {getFieldDecorator('gender', {
                                rules: [{ required: true, message: 'Please enter gender' }],
                            })(
                                <Select labelInValue  
                                placeholder={allStrings.gender}
                                style={{ width: '100%'}} onChange={handleChange}>
                                    <Option value="male">{allStrings.male}</Option>
                                    <Option value="female">{allStrings.female}</Option>
                                </Select>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.balance}</label>
                            <Form.Item>
                            {getFieldDecorator('balance', {
                                rules: [{ required: true, message: 'Please enter balacne' }],
                                initialValue:user.balance
                            })(
                                <Input />
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.rate}</label>
                            <Form.Item>
                            {getFieldDecorator('rate', {
                                rules: [{ required: true, message: 'Please enter rate' }],
                                initialValue:user.rate
                            })(
                                <Input />
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.ratePercent}</label>
                            <Form.Item>
                            {getFieldDecorator('ratePercent', {
                                rules: [{ required: true, message: 'Please enter rate percent' }],
                                initialValue:user.ratePercent
                            })(
                                <Input />
                            )}
                            </Form.Item>
                            
                            </Form>
                            <label for="name" class="lab">{allStrings.personalImage}</label>
                            <br/>
                            <input className='profileImg' type="file" onChange= {this.onChange}></input>
                        </Modal>
                        <Modal
                            title={allStrings.orders}
                            visible={this.state.ordermodal}                          
                            okText={allStrings.ok}
                            cancelText={allStrings.cancel}
                            onCancel={() => this.setState({ordermodal:false})}
                        >

                          

                          <div className='orderTypes'>
                            <button onClick={()=>{
                              this.getOrders(1,'PENDING',true)
                              this.page=1;
                              this.type='PENDING'
                            }}
                            >{allStrings.pendingOrder}</button>

                            <button onClick={()=>{
                              this.getOrders(1,'CANCEL',true)
                              this.page=1;
                              this.type='CANCEL'
                            }}
                            >{allStrings.canceledOrder}</button>

                            <button
                            onClick={()=>{
                              this.getOrders(1,'ON_PROGRESS',true)
                              this.page=1;
                              this.type='ON_PROGRESS'
                            }}
                            >{allStrings.onprogressOrder}</button>

                            <button
                            onClick={()=>{
                              this.getOrders(1,'DELIVERED',true)
                              this.page=1;
                              this.type='DELIVERED'
                            }}
                            >{allStrings.deliveredOrder}</button>

                            

                          </div>
                          {this.state.selectFlag?
                          <table>
                            <tr  className='tableHeader'>
                              <th>{allStrings.name}</th>
                              <th>{allStrings.phone}</th>
                              <th>{allStrings.order}</th>
                              <th>{allStrings.time}</th>
                              <th>{allStrings.shopName}</th>
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{allStrings.salesmanName}</th>
                              }
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{allStrings.salesmanPhone}</th>
                              }
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{allStrings.offerPrice}</th>
                              }
                            </tr>
                            
                            {this.state.orders.map(order=>
                            <tr onClick={()=>{
                              this.props.history.push('/OrderInfo',{data:order})
                            }} className='tableContent'>
                              <th>{order.client.username}</th>
                              <th>{order.client.phone}</th>
                              <th>{order.description}</th>
                              <th>{order.time}</th>
                              <th>{order.shopName}</th>
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{order.salesMan.username}</th>
                              }
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{order.salesMan.phone}</th>
                              }
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{order.offer.deliveryCost}</th>
                              }
                            </tr>
                            )}
                            
                          </table>
                          :
                          <div className='selectType'> 
                          <span >{allStrings.selectType}</span>
                          </div>
                          }
                          {this.state.selectFlag&& 
                          <div className='orderLoadMore'>
                            <button onClick={()=>{
                              this.getOrders(this.page+1);
                              this.page++;
                            }}>{allStrings.loadMore}</button>
                          </div>
                          }
                          </Modal>       
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
    getUser,
  }


export default connect(mapToStateProps,mapDispatchToProps) ( UserInfo = Form.create({ name: 'normal_login' })(UserInfo)) ;
