import React, { Component } from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './salesMan.css';
//import {Icon,Button,Modal,Input} from 'react-materialize';
import {Skeleton,DatePicker, Modal, Form, Icon, Input, Button,Select,Popconfirm, message} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'


class SalesMan extends React.Component {
   

    constructor(props){
        super(props)
        this.getSalesMen(1)
        this.getYears();
        this.getTypes();
        this.getBanks()
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }
      pagentationPage=0;
      counter=0;
    state = {
        visible: false,
        confirmDelete: false,
        selectedUser:null,
        salesmen:[],
        loading:true,
        file:null,
        showFile:null,
        file2:null,
        showFile2:null,
        file3:null,
        showFile3:null,
        years: [],
        types: [],
        banks: [],
        }


        getYears = () => {
          axios.get(`${BASE_END_POINT}year`)
          .then(response=>{
            this.setState({years:response.data.data})
          })
          .catch(error=>{
            //console.log("ALL Categories ERROR")
            //console.log(error.response)
          })
        }

        getTypes = () => {
          axios.get(`${BASE_END_POINT}type`)
          .then(response=>{
            this.setState({types:response.data.data})
          })
          .catch(error=>{
            //console.log("ALL Categories ERROR")
            //console.log(error.response)
          })
        }

        getBanks = () => {
          axios.get(`${BASE_END_POINT}bank`)
          .then(response=>{
            this.setState({banks:response.data.data})
          })
          .catch(error=>{
            //console.log("ALL Categories ERROR")
            //console.log(error.response)
          })
        }

       //submit form
       flag = -1;
       getSalesMen = (page,deleteRow) => {
        axios.get(`${BASE_END_POINT}getAll?type=SALES-MAN&page=${page}&limit={20}`)  
        .then(response=>{
           console.log("ALL salesmen")
           console.log(response.data.data)
           this.setState({salesmen:deleteRow?response.data.data:[...this.state.salesmen,...response.data.data],loading:false})
         })
         .catch(error=>{
          // console.log("ALL salesmen ERROR")
          // console.log(error.response)
         })
       }

       OKBUTTON = (e) => {
        this.deleteUser()
       }
 
       deleteUser = () => {
         let l = message.loading(allStrings.wait, 2.5)
         axios.delete(`${BASE_END_POINT}${this.state.selectedUser}/delete`,{
           headers: {
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
         .then(response=>{
             l.then(() => message.success(allStrings.deleteDone, 2.5))
             this.getSalesMen(1,true)
             this.flag = -1
             
         })
         .catch(error=>{
            // console.log(error.response)
             l.then(() => message.error('Error', 2.5))
         })
      }

    onChange = (e) => {
      this.setState({file:e.target.files[0],
        showFile:URL.createObjectURL(e.target.files[0])
      });      
    }

    onChange2 = (e) => {
      this.setState({file2:e.target.files,
        //showFile2:URL.createObjectURL(e.target.files[0])
      });
    }

    onChange3 = (e) => {
      this.setState({file3:e.target.files,
       // showFile3:URL.createObjectURL(e.target.files[0])
      });     
    }
    //submit form
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            //console.log('Received values of form: ', values);
            let date  = values.birthYear._d.toLocaleDateString().split('/')
            var form = new FormData();
            form.append('img',this.state.file);
            for(var i=0 ; i<= this.state.file2.length-1 ; i++)
            {
                form.append(`transportLicense`,this.state.file2[i] )
            }
            for(var i=0 ; i<= this.state.file3.length-1 ; i++)
            {
                form.append(`transportImages`,this.state.file3[i] )
            }
            //form.append('transportLicense',this.state.file2);
            //form.append('transportImages',this.state.file3);
            form.append('username',values.username);
            form.append('email',values.email);
            form.append('phone',values.phone);
            form.append('gender',values.gender.key);
            form.append('birthYear',date[2]);
            form.append('type','SALES-MAN');
            form.append('transportType',values.transportType.key);
            form.append('manufacturingYear',values.manufacturingYear.key);
            form.append('bank',values.bank.key);
            form.append('bankNumber',values.bankNumber);
            form.append('signUpFrom',values.signUpFrom);
            
            let l = message.loading(allStrings.wait, 2.5)
            axios.post(`${BASE_END_POINT}addSalesMan`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.addDone, 2.5));
                this.setState({ modal1Visible:false,file:null,file2:null,file3:null });
                this.fileInput.value = "";
                this.fileInput2.value = "";
                this.fileInput3.value = "";
                this.getSalesMen(1,true)
                this.flag = -1
                this.props.form.resetFields()
            })
            .catch(error=>{
                console.log("error in add salesman    ",error.response)
                l.then(() => message.error('duplicated data in email or phone', 2.5))
            })
          }
        });
        
      }
     
      

    showModal = () => {
      this.setState({
        visible: true,
      });
    }
  
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

      validatePhone = (rule, value, callback) => {
        const { form } = this.props;
        if ( isNaN(value) ) {
          callback('Please enter correct phone');
        }else if ( value.length <11 ) {
          callback('Phone number must be grater than 11 digit');
        }
         else {
          callback();
        }
      };

  //end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
         let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
         let list = this.state.salesmen.map(
           val=>[val.id,val.username,val.phone,val.email,""+val.hasCoupon,""+val.rate,""+val.ratePercent,
           ""+val.tasksCount,""+val.balance,controls])
         
           const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const {select} = this.props;
      return (
          <div>
               <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
              <Tables columns={this.state.loading?['Loading...']:[allStrings.id,allStrings.name, allStrings.phone,allStrings.email,allStrings.hasCopon,allStrings.rate,allStrings.ratePercent,allStrings.taskCount,allStrings.balance,allStrings.remove]} title={allStrings.salesmantable}
              arr={this.state.loading?loadingView:list}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
               // console.log('row index   '+colData)
                if(cellMeta.colIndex!=8){            
                 console.log(this.state.salesmen[cellMeta.rowIndex])
                  this.props.history.push('/SalesManInfo',{data:this.state.salesmen[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex==8){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedUser:id})
                    //console.log(id)
                  }
              }}

               onChangePage={(currentPage)=>{
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getSalesMen(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              >
              </Tables>
              
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addsalesman}</Button>
              <Modal
                    title="Add"
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    cancelText={allStrings.cancel}
                    okText={allStrings.ok}
                    onCancel={() => this.setModal1Visible(false)}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                    {getFieldDecorator('username', {
                        rules: [{ required: true, message: 'Please enter  Name' }],
                    })(
                        <Input placeholder={allStrings.name} />
                    )}
                    </Form.Item>
                                                        
                    <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [
                          { required: true, message: 'Please enter email' },
                          {type: 'email',  message: 'Please enter correct email' }
                        ],
                    })(
                        <Input placeholder={allStrings.email} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('phone', {
                        rules: [
                          { required: true, message: 'Please enter phone' },
                          {
                            validator: this.validatePhone,
                          },
                      ],
                    })(
                        <Input placeholder={allStrings.phone} />
                    )}
                    </Form.Item>
                  
                    <Form.Item>
                      {getFieldDecorator('gender', {
                        rules: [{ required: true, message: 'Please enter gender' }],
                    })(
                        <Select labelInValue  
                        placeholder={allStrings.gender}
                        style={{ width: '100%'}} onChange={handleChange}>
                            <Option value="male">{allStrings.male}</Option>
                            <Option value="female">{allStrings.female}</Option>
                        </Select>
                    )}
                    </Form.Item>



                    <Form.Item>
                    {getFieldDecorator('birthYear', {
                        rules: [{ required: true, message: 'Please enter birth Year' }],
                    })(
                      <DatePicker style={{ width: '100%'}}/>
                    )}
                    </Form.Item>
                   
                    <Form.Item>
                    {getFieldDecorator('signUpFrom', {
                        rules: [{ required: true, message: 'Please enter card signup from' }],
                    })(
                        <Input placeholder={allStrings.signUpFrom} />
                    )}
                    </Form.Item>
                    
                  

                    <Form.Item>
                    {getFieldDecorator('transportType', {
                        rules: [{ required: true, message: 'Please enter transportType' }],
                    })(
                      <Select labelInValue  
                      placeholder={allStrings.transportType}
                      style={{ width: '100%'}} onChange={handleChange}>
                           {this.state.types.map(val=>
                           <Option value={val.Type}>{this.props.isRTL?val.arabicType:val.Type}</Option>
                           )}                 
                      </Select>
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('manufacturingYear', {
                        rules: [{ required: true, message: 'Please enter  manufacturingYear' }],
                    })(
                      <Select labelInValue  
                      placeholder={allStrings.manufacturingYear}
                      style={{ width: '100%'}} onChange={handleChange}>
                           {this.state.years.map(val=>
                           <Option value={val.Year}>{val.Year}</Option>
                           )}                 
                      </Select>
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('bank', {
                        rules: [{ required: true, message: 'Please enter bank' }],
                    })(
                      <Select labelInValue  
                      placeholder={allStrings.bank}
                      style={{ width: '100%'}} onChange={handleChange}>
                           {this.state.banks.map(val=>
                           <Option value={val.Bank}>{this.props.isRTL?val.arabicBank:val.Bank}</Option>
                           )}                 
                      </Select>
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('bankNumber', {
                        rules: [{ required: true, message: 'Please enter bankNumber' }],
                    })(
                        <Input placeholder={allStrings.bankNumber} />
                    )}
                    </Form.Item><label> اضف صورتك الشخصية </label>

                    <div>
                      
                      <div>
                      <Form.Item>
                        {getFieldDecorator('img', {
                            rules: [{ required: true, message: 'Please upload img' }],
                        })(
                          <input ref={ref=> this.fileInput = ref} value='img' type="file" onChange= {this.onChange} multiple></input>
                        )}
                            
                    </Form.Item>
                    {/*
                    <img style={{border:'2px solid gray',borderRadius:5, height:100,width:100}} src={this.state.showFile} />
                    */}
                    </div>
                    </div>
                   
                    <div style={{marginTop:20}}>
                      <label> اضف صور من رخصة سيارتك </label>
                      <div>
                    <Form.Item>
                        {getFieldDecorator('transportLicense', {
                            rules: [{ required: true, message: 'Please upload transport License' }],
                        })(
                          <input ref={ref=> this.fileInput2 = ref} multiple value='transport License' type="file" onChange= {this.onChange2} multiple></input>
                        )}
                            
                    </Form.Item>
                    {/*
                    <img style={{border:'2px solid gray',borderRadius:5, height:100,width:100}} src={this.state.showFile2} />
                    */}
                    </div>
                    </div>


                    <div style={{marginTop:20}}>
                      <label> اضف صور لسيارتك </label>
                      <div>
                    <Form.Item>
                        {getFieldDecorator('transportImages', {
                            rules: [{ required: true, message: 'Please upload transport Images' }],
                        })(
                          <input ref={ref=> this.fileInput3 = ref} multiple type="file" onChange= {this.onChange3} multiple></input>
                        )}
                            
                    </Form.Item>
                    {/*
                    <img style={{border:'2px solid gray',borderRadius:5, height:100,width:100}} src={this.state.showFile3} />
                    */}
                    </div>
                    </div>

                    </Form>
                </Modal>
           
              </div>
              </div>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,


  })
  
  const mapDispatchToProps = {
  }

  export default withRouter( connect(mapToStateProps,mapDispatchToProps) (SalesMan = Form.create({ name: 'normal_login' })(SalesMan)) );

