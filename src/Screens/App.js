import React, { Component } from 'react';
import Dashboard from './dashboard/dashboard';
import Users from './users/users';
import Category from './category/category';
import ServicesCategory from './services category/services category';

import Bank from './bank/bank';
import Type from './type/type';

import Shops from './shops/shops';
import Terms from './terms/terms';
import Time from './time/time';
import Coupon from './coupon/coupon';
import PendingOrder from './order/order';
import Report from './report/report';
import Login from './login/login';
import SalesMan from './salesMan/salesMan'
import Contactus from './contactus/contactus'


import OrderInfo from './order info/order info'
import AdminInfo from './admin info/admin info'
import TimeInfo from './time info/time info'
import BankInfo from './bank info/bank info'

import CouponInfo from './coupon info/coupon info'
import SalesManInfo from './salesMan info/salesMan info'
import SalesManInfoNoti from './salesMan info/salesMan info noti'
import UserInfo from './user info/user info'
import Splash from './splash/splash'
import Year from './year/year'
import CategoryInfo from './category info/category info'
import ShopInfo from './shop info/shop info'

import TypeInfo from './type info/type info'
import OrderInfoFronNoti from './order info/OrderInfoFronNoti'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import {askForPermissioToReceiveNotifications} from '../config/push-notification';
import { Provider } from 'react-redux'
import {store,persistor } from '../store';
import { PersistGate } from 'redux-persist/integration/react'
import timeInfo from './time info/time info';
import YearInfo from './year info/year info';

class App extends Component {
  componentDidMount(){
   // askForPermissioToReceiveNotifications()
  }
  //
  render() {
    return (
      <Provider store={store}>
         <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <div className="App">
          <Switch>
            <Route path='/Dashboard' component={Dashboard}/>
            <Route  path='/Login' component={Login}/>
            <Route  path='/year' component={Year}/>
            <Route  path='/YearInfo' component={YearInfo}/>
            <Route path='/Contactus' component={Contactus}/>
            <Route exact path='/' component={Splash}/>
            <Route path='/users' component={Users} />
            <Route path='/bank' component={Bank} />
            <Route path='/TypeInfo' component={TypeInfo} />
            <Route path='/type' component={Type} />
            <Route path='/time' component={Time} />
            <Route path='/terms' component={Terms} />
            <Route path='/coupon' component={Coupon} />
            <Route path='/sales-man' component={SalesMan} />
            <Route path='/SalesManInfoNoti' component={SalesManInfoNoti} />
            <Route path='/BankInfo' component={BankInfo} />
            <Route path='/pendingOrder' component={PendingOrder} />
            <Route path='/category' component={Category} />
            <Route path='/services category' component={ServicesCategory} />
            <Route path='/shops' component={Shops} />
            <Route path='/reports' component={Report} />
            <Route path='/OrderInfo' component={OrderInfo} />
            <Route path='/AdminInfo' component={AdminInfo} />
            <Route path='/TimeInfo' component={timeInfo} />
            <Route path='/SalesManInfo' component={SalesManInfo} />
            <Route path='/UserInfo' component={UserInfo} />
            <Route path='/CouponInfo' component={CouponInfo} />
            <Route path='/CategoryInfo' component={CategoryInfo} />
            <Route path='/ShopInfo' component={ShopInfo} />
            <Route path='/OrderInfoFronNoti' component={OrderInfoFronNoti} />

          </Switch>
        </div>
      </BrowserRouter>
      </PersistGate>
      </Provider>
    );
  }
}

export default App;
