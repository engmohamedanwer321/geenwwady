import { message} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {
  LOGIN_REQUEST,LOGIN_SUCCESS,USER_TOKEN,CURRENT_USER
   } from './types';
import { BASE_END_POINT } from '../config/URL';


//
export function login(phone, token, history) {
 console.log(phone,"    ",token)
  return (dispatch) => {    
    dispatch({ type: LOGIN_REQUEST });
    let l = message.loading('انتظر...', 2.5)
    axios.post(`${BASE_END_POINT}signin`, JSON.stringify({
      phone:phone,
      token:token
    }), {
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(res => {
      l.then(() => message.success('مرحبا', 2.5))  
      console.log('login Done  ',res.data);   
      localStorage.setItem('@QsathaUser', JSON.stringify(res.data));  
      dispatch({ type: LOGIN_SUCCESS, payload: res.data});  
        history.push('/Dashboard') 
    
    })
      .catch(error => {
        l.then(() => message.error('رقم الهاتف غير مسجل فى قاعدة البيانات', 2.5))
        console.log('outer');
        console.log(error.response);
      });
  };
}

export function getUser(user){
  return dispatch => {
    dispatch({ type: CURRENT_USER, payload: user });
  }
}


export function userToken(token){
  //console.log("hi  "+token)
  return dispatch => {
    dispatch({type:USER_TOKEN,payload:token})
  }
}

/*export function userLocation(postion) {
  return dispatch => {
    console.log(postion)
    dispatch({type:USER_LOCATION,payload:postion})
  }
  
}
*/
